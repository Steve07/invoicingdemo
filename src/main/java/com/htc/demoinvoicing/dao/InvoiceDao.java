package com.htc.demoinvoicing.dao;

import java.util.List;

import com.htc.demoinvoicing.model.InvoiceModel;

public interface InvoiceDao {

	boolean insert(InvoiceModel invo);

	Integer getID();

	boolean delete(long invo_id);

	boolean modify(long factura_id, InvoiceModel invo);

	List<InvoiceModel> loadAllInvoice();

	InvoiceModel findInvoiceById(long invo_id);

}
