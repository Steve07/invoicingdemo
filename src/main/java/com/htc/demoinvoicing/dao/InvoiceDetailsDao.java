package com.htc.demoinvoicing.dao;

import java.util.List;

import com.htc.demoinvoicing.model.InvoiceDetailsModel;

public interface InvoiceDetailsDao {

	boolean insert(InvoiceDetailsModel invod);

	void inserBatch(List<InvoiceDetailsModel> invoicedetails);

	boolean modify(long factura_id, InvoiceDetailsModel invod);

	List<InvoiceDetailsModel> loadAllInvoiceDetails();

	List<InvoiceDetailsModel> findInvoiceDetailsById(long invo_id);

}
