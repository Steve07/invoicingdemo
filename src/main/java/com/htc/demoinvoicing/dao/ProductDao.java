package com.htc.demoinvoicing.dao;

import java.util.List;

import com.htc.demoinvoicing.model.Product;

public interface ProductDao {

	boolean insert(Product invo);
	
	void inserBatch(List<Product> products);

	boolean delete(long factura_id, Product prod);

	boolean modify(long factura_id, Product prod);

	List<Product> loadAllProduct();

	Product findProductById(long product_id);

	

}
