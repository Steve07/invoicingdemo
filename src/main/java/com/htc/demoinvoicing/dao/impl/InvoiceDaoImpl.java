package com.htc.demoinvoicing.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import com.htc.demoinvoicing.dao.InvoiceDao;
import com.htc.demoinvoicing.model.InvoiceModel;

@Repository
public class InvoiceDaoImpl extends JdbcDaoSupport implements InvoiceDao {
	String sql;

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(InvoiceModel invo) {
		// se hace la consulta directa y se le pasan los campos,
		// validamos que la consulta se haga de manera correcta con un return > 0
		sql = "INSERT INTO invoice (invoice_id, customer_name,date,total,iva, total_payment,status) VALUES (?,?,?,?,?,?,?)";
		return getJdbcTemplate().update(sql, new Object[] { invo.getInvoiceId(), invo.getCustomerName(),
				invo.getDateInfo(), invo.getTotal(), invo.getIva(), invo.getTotalPayment(), invo.getStatus() }) > 0;
	}

	@Override
	public List<InvoiceModel> loadAllInvoice() {
		sql = "SELECT * FROM invoice";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<InvoiceModel> result = new ArrayList<InvoiceModel>();
		for (Map<String, Object> row : rows) {
			InvoiceModel invo = new InvoiceModel();
			invo.setInvoiceId((Integer) row.get("invoice_id"));
			invo.setDateInfo((String) row.get("date"));
			invo.setCustomerName((String) row.get("customer_name"));
			invo.setTotal((double) row.get("iva"));
			invo.setTotal((double) row.get("total"));
			invo.setTotalPayment((double) row.get("total_payment"));
			result.add(invo);
		}
		return result;
	}

	@Override
	public boolean modify(long factura_id, InvoiceModel invo) {
		sql = "UPDATE invoice SET customer_name = ? WHERE invoice_id =?";
		return getJdbcTemplate().update(sql, new Object[] { invo.getCustomerName(), factura_id }) > 0;
	}

	@Override
	public boolean delete(long invo_id) {

		sql = "UPDATE invoice SET status=false WHERE invoice_id = ?";
		return getJdbcTemplate().update(sql, new Object[] { invo_id }) > 0;
	}

	@Override
	public Integer getID() {
		// se hace la consulta a la secuencia y se le coloca un alias
		int result = 0;
		sql = "select nextval('sec_invoice_id') as secinvoice";
		List<Integer> temp = getJdbcTemplate().query(sql, (rs, rowNum) -> {
			return rs.getInt("secinvoice");
		});
		if (temp.size() > 0) {
			result = temp.get(0);
		}
		return result;
	}

	@Override
	public InvoiceModel findInvoiceById(long invo_id) {
		InvoiceModel invoice = null;
		sql = "SELECT * FROM invoice WHERE invoice_id = ? and status=true";
		List<InvoiceModel> temp = getJdbcTemplate().query(sql, new Object[] { invo_id }, (rs, rowNum) -> {
			InvoiceModel invo = new InvoiceModel();
			invo.setInvoiceId(rs.getLong("invoice_id"));
			invo.setDateInfo(rs.getString("date"));
			invo.setCustomerName(rs.getString("customer_name"));
			invo.setTotal(rs.getDouble("total"));
			invo.setIva(rs.getDouble("iva"));
			invo.setTotalPayment(rs.getDouble("total_payment"));
			return invo;
		});

		if (temp == null || temp.isEmpty()) {
			return null;
		}
		return temp.get(0);

	}

}
