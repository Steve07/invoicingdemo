package com.htc.demoinvoicing.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.htc.demoinvoicing.dao.ProductDao;
import com.htc.demoinvoicing.model.Product;

@Repository
public class ProductDaoImpl extends JdbcDaoSupport implements ProductDao {
	String sql;
	public int id;
	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(Product invo) {
		// TODO Auto-generated method stub
		sql = "INSERT INTO product " + "(product_name, price,stock) VALUES (?,?,?,?)";
		return getJdbcTemplate().update(sql, new Object[] { invo.getProductName(),
				invo.getPrice(), invo.getStock(), invo.getStatus() }) > 0;
	}

	@Override
	public void inserBatch(List<Product> prod) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean delete(long factura_id, Product prod) {
		 sql = "UPDATE product SET status = ? WHERE product_id =?";
		return getJdbcTemplate().update(sql, new Object[] { prod.getStatus(), factura_id }) > 0;
	}

	@Override
	public boolean modify(long product_id, Product prod) {
		// TODO Auto-generated method stub
		 sql = "UPDATE product  SET stock = stock-? WHERE product_id =?";
		return getJdbcTemplate().update(sql, new Object[] { prod.getStock(), product_id }) > 0;
	}

	@Override
	public List<Product> loadAllProduct() {
		// TODO Auto-generated method stub
		 sql = "SELECT * FROM product where status=true";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
		List<Product> result = new ArrayList<Product>();
		for (Map<String, Object> row : rows) {
			Product invo = new Product();
			invo.setProductId((Integer) row.get("product_id"));
			invo.setProductName((String) row.get("product_name"));
			invo.setPrice((Double) row.get("price"));
			invo.setStock((Integer) row.get("stock"));
			result.add(invo);
		}
		return result;
	}

	@Override
	public Product findProductById(long product_id) {
		 sql = "SELECT * FROM product WHERE product_id = ?";
		List<Product> temp = getJdbcTemplate().query(sql, new Object[] { product_id }, (rs, rowNum) -> {
			Product prod = new Product();
			prod.setProductId(rs.getLong("product_id"));
			prod.setProductName(rs.getString("product_name"));
			prod.setPrice(rs.getDouble("price"));
			prod.setStock(rs.getInt("stock"));
			return prod;
		});
		if (temp == null || temp.isEmpty()) {
			return null;
		}
		return temp.get(0);
	}
}
