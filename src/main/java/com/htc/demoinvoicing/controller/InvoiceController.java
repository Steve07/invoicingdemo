package com.htc.demoinvoicing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;

import com.htc.demoinvoicing.invoice.AddInvoiceRequest;
import com.htc.demoinvoicing.invoice.AddInvoiceResponse;
import com.htc.demoinvoicing.invoice.DeleteInvoiceRequest;
import com.htc.demoinvoicing.invoice.DeleteInvoiceResponse;
import com.htc.demoinvoicing.invoice.GetInvoiceByIdRequest;
import com.htc.demoinvoicing.invoice.GetInvoiceByIdResponse;
import com.htc.demoinvoicing.service.impl.MethosInvoiceService;
import com.htc.demoinvoicing.utils.ExceptionMessages;

@RestController
@RequestMapping("/invoices")
public class InvoiceController {

	private MethosInvoiceService service;
	
	@Autowired
	public InvoiceController(MethosInvoiceService service) {
		this.service = service;
	}
	
	@PostMapping("/")
	public ResponseEntity<AddInvoiceResponse> addInvoice(@RequestBody AddInvoiceRequest request) {
		try {
			AddInvoiceResponse add = service.addInvoice(request);
			if (add.getServiceStatus().getStatusCode().equals("201")) {
				return new ResponseEntity<AddInvoiceResponse>(add, HttpStatus.OK);
			} else {
				return new ResponseEntity<AddInvoiceResponse>(add, HttpStatus.CONFLICT);
			}
		} catch (ExceptionMessages e) {
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("/{invoiceid}")
	public ResponseEntity<GetInvoiceByIdResponse> getAllInvoice(@RequestPayload GetInvoiceByIdRequest request) {
		GetInvoiceByIdResponse getinvo = service.getInvoiceById(request);
		if (getinvo.getServiceStatus().getStatusCode().equals("200")) {
			return new ResponseEntity<GetInvoiceByIdResponse>(getinvo, HttpStatus.OK);
		} else {
			return new ResponseEntity<GetInvoiceByIdResponse>(getinvo, HttpStatus.CONFLICT);
		}
	}

	@DeleteMapping("/{invoiceid}")
	public ResponseEntity<DeleteInvoiceResponse> deleteInvoice(@RequestPayload DeleteInvoiceRequest request) {
		DeleteInvoiceResponse invo = service.delete(request);
		if (invo.getServiceStatus().getStatusCode().equals("204")) {
			return new ResponseEntity<DeleteInvoiceResponse>(invo, HttpStatus.OK);
		} else {
			return new ResponseEntity<DeleteInvoiceResponse>(invo, HttpStatus.CONFLICT);
		}
	}
}
