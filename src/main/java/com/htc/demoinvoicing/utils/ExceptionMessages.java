package com.htc.demoinvoicing.utils;

public class ExceptionMessages extends Exception {
	
	private static final long serialVersionUID = 1L;
	private String code;
	private String description;
	
	public ExceptionMessages() {
		
	}
	
	public ExceptionMessages(String code) {
		this.code=code;
	}
	public ExceptionMessages(String code, String description) {
		this.code=code;
		this.description=description;
	}

	public ExceptionMessages(String description, String code, Exception ex) {
	    super(ex);
	    this.code = code;
	    this.description = description;
	  }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
