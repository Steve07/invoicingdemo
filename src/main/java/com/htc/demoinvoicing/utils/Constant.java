package com.htc.demoinvoicing.utils;

public class Constant {
	
	public static final String SUCCESS_CODE="200";
	public static final String SUCCESS_CODE_CREATED="201";
	public static final String SUCCESS_CODE_DETELE="204";
	public static final String SUCCESS_INVOICE="Factura guardada con exito";
	public static final String SUCCESS_DELETE_INVOICE="La factura se eliminó";
	
	public static final String ERROR_CODE="408";
	public static final String NOT_STOCK="Cantidad no disponible";
	public static final String PRODUCT_QUANTITY="La cantidad tiene que ser mayor a 0";
	public static final String NULL_ERROR="Tados nulos";
	public static final String NOT_INVOICE="Factura no encontrada";
	public static final String ERROR_NOT_IVOICE="Factura no encontrada";
	public static final String NOT_PRODUCT="Producto no encontrado";
	
	public static final String ERROR_CODE_DELETE="400";
	public static final String ERROR_INVOICE="Error al guardar";
	public static final String ERROR_DELETE="Error al eliminar";
	
	
	public static final double IVA=0.13;
}
