//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.05.28 a las 02:52:11 PM CST 
//


package com.htc.demoinvoicing.invoice;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.htc.demoinvoicing.invoice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.htc.demoinvoicing.invoice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetInvoiceByIdRequest }
     * 
     */
    public GetInvoiceByIdRequest createGetInvoiceByIdRequest() {
        return new GetInvoiceByIdRequest();
    }

    /**
     * Create an instance of {@link AddInvoiceResponse }
     * 
     */
    public AddInvoiceResponse createAddInvoiceResponse() {
        return new AddInvoiceResponse();
    }

    /**
     * Create an instance of {@link ServiceStatus }
     * 
     */
    public ServiceStatus createServiceStatus() {
        return new ServiceStatus();
    }

    /**
     * Create an instance of {@link DeleteInvoiceRequest }
     * 
     */
    public DeleteInvoiceRequest createDeleteInvoiceRequest() {
        return new DeleteInvoiceRequest();
    }

    /**
     * Create an instance of {@link AddInvoiceRequest }
     * 
     */
    public AddInvoiceRequest createAddInvoiceRequest() {
        return new AddInvoiceRequest();
    }

    /**
     * Create an instance of {@link InvoiceDetailsDto }
     * 
     */
    public InvoiceDetailsDto createInvoiceDetailsDto() {
        return new InvoiceDetailsDto();
    }

    /**
     * Create an instance of {@link DeleteInvoiceResponse }
     * 
     */
    public DeleteInvoiceResponse createDeleteInvoiceResponse() {
        return new DeleteInvoiceResponse();
    }

    /**
     * Create an instance of {@link GetInvoiceByIdResponse }
     * 
     */
    public GetInvoiceByIdResponse createGetInvoiceByIdResponse() {
        return new GetInvoiceByIdResponse();
    }

}
