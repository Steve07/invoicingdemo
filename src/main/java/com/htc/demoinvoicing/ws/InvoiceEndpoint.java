package com.htc.demoinvoicing.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.htc.demoinvoicing.invoice.AddInvoiceRequest;
import com.htc.demoinvoicing.invoice.AddInvoiceResponse;
import com.htc.demoinvoicing.invoice.DeleteInvoiceRequest;
import com.htc.demoinvoicing.invoice.DeleteInvoiceResponse;
import com.htc.demoinvoicing.invoice.GetInvoiceByIdRequest;
import com.htc.demoinvoicing.invoice.GetInvoiceByIdResponse;
import com.htc.demoinvoicing.service.impl.MethosInvoiceService;
import com.htc.demoinvoicing.utils.ExceptionMessages;

@Endpoint
public class InvoiceEndpoint {
	public static final String NAMESPACE_URI = "http://www.demoinvoicing.htc.com/Invoice";
	@Autowired
	private MethosInvoiceService service;
	
	public InvoiceEndpoint() {

	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addInvoiceRequest")
	@ResponsePayload
	public AddInvoiceResponse addInvoice(@RequestPayload AddInvoiceRequest request) throws ExceptionMessages {
		return	service.addInvoice(request);
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteInvoiceRequest")
	@ResponsePayload
	public DeleteInvoiceResponse deleteInvoice(@RequestPayload DeleteInvoiceRequest request) {
		return service.delete(request);
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getInvoiceByIdRequest")
	@ResponsePayload
	public GetInvoiceByIdResponse getAllInvoice(@RequestPayload GetInvoiceByIdRequest request) {
		return service.getInvoiceById(request);
	}

}
