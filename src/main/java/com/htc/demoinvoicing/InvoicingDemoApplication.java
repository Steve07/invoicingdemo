package com.htc.demoinvoicing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.htc.demoinvoicing.service.impl.InvoiceDetailsServiceImpl;
import com.htc.demoinvoicing.service.impl.InvoiceServiceImpl;
import com.htc.demoinvoicing.service.impl.MethosInvoiceService;

@SpringBootApplication
public class InvoicingDemoApplication implements CommandLineRunner {
	@Autowired
	private InvoiceServiceImpl service;
	@Autowired
	private InvoiceDetailsServiceImpl serviceimpl;
	
	public static void main(String[] args) {
		SpringApplication.run(InvoicingDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
	
	}

}
