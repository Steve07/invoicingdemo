package com.htc.demoinvoicing.service;

import java.util.List;

import com.htc.demoinvoicing.model.InvoiceDetailsModel;

public interface InvoiceDetailsService {
	
	boolean insert(InvoiceDetailsModel invod);

	boolean modify(InvoiceDetailsModel invo);
	
	void inserBatch(List<InvoiceDetailsModel> invoices);

	List<InvoiceDetailsModel> loadAllInvoiceDetails();

	List<InvoiceDetailsModel> findInvoiceDetailsById(long invo_id);
}
