package com.htc.demoinvoicing.service;

import java.util.List;

import com.htc.demoinvoicing.model.Product;

public interface ProductService {

	boolean insert(Product prod);

	boolean delete(Product prod);

	boolean modify(Product prod);

	void inserBatch(List<Product> products);

	List<Product> loadAllProduct();

	Product findProductById(long product_id);
}