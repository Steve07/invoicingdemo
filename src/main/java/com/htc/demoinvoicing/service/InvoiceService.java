package com.htc.demoinvoicing.service;

import java.util.List;

import com.htc.demoinvoicing.model.InvoiceModel;

public interface InvoiceService {

	boolean insert(InvoiceModel invo);

	Integer getID();

	boolean delete(long invo_id);

	boolean modify(InvoiceModel invo);

	List<InvoiceModel> loadAllInvoice();

	InvoiceModel findInvoiceById(long invo_id);

}