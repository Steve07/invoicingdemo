package com.htc.demoinvoicing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.demoinvoicing.dao.InvoiceDetailsDao;
import com.htc.demoinvoicing.model.InvoiceDetailsModel;
import com.htc.demoinvoicing.service.InvoiceDetailsService;

@Service
public class InvoiceDetailsServiceImpl implements InvoiceDetailsService{
	
	 @Autowired 
	 InvoiceDetailsDao invoicedetailsDao;
	 

	@Override
	public boolean insert(InvoiceDetailsModel invod) {
		
		return invoicedetailsDao.insert(invod);
	}

	@Override
	public boolean modify(InvoiceDetailsModel invo) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void inserBatch(List<InvoiceDetailsModel> invoices) {
		// TODO Auto-generated method stub
		invoicedetailsDao.inserBatch(invoices);
	}

	@Override
	public List<InvoiceDetailsModel> loadAllInvoiceDetails() {
		// TODO Auto-generated method stub
		List<InvoiceDetailsModel> listInvoDe = invoicedetailsDao.loadAllInvoiceDetails();
	   return listInvoDe;	
	}

	@Override
	public List<InvoiceDetailsModel> findInvoiceDetailsById(long invo_id) {
		// TODO Auto-generated method stub
		List<InvoiceDetailsModel> listInvoDe =invoicedetailsDao.findInvoiceDetailsById(invo_id);
		 return listInvoDe;
	}

}
