package com.htc.demoinvoicing.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.htc.demoinvoicing.invoice.AddInvoiceRequest;
import com.htc.demoinvoicing.invoice.AddInvoiceResponse;
import com.htc.demoinvoicing.invoice.DeleteInvoiceRequest;
import com.htc.demoinvoicing.invoice.DeleteInvoiceResponse;
import com.htc.demoinvoicing.invoice.GetInvoiceByIdRequest;
import com.htc.demoinvoicing.invoice.GetInvoiceByIdResponse;
import com.htc.demoinvoicing.invoice.InvoiceDetailsDto;
import com.htc.demoinvoicing.invoice.ServiceStatus;
import com.htc.demoinvoicing.model.InvoiceDetailsModel;
import com.htc.demoinvoicing.model.InvoiceModel;
import com.htc.demoinvoicing.model.Product;
import com.htc.demoinvoicing.service.InvoiceDetailsService;
import com.htc.demoinvoicing.service.InvoiceService;
import com.htc.demoinvoicing.service.ProductService;
import com.htc.demoinvoicing.utils.Constant;
import com.htc.demoinvoicing.utils.ExceptionMessages;

@Service
public class MethosInvoiceService {

	@Autowired
	InvoiceService invoiceService;

	@Autowired
	InvoiceDetailsService invoicedetailsService;

	@Autowired
	ProductService Produc;

	private static final Logger LOG = LoggerFactory.getLogger(MethosInvoiceService.class);

	public GetInvoiceByIdResponse getInvoiceById(GetInvoiceByIdRequest request) {
		GetInvoiceByIdResponse response = new GetInvoiceByIdResponse();
		ServiceStatus serviceStatus = new ServiceStatus();
		InvoiceModel invoiceEntity = invoiceService.findInvoiceById(request.getInvoiceId());
		if (invoiceEntity == null) {
			LOG.info("No se encontro la factura");
			serviceStatus.setStatusCode(Constant.ERROR_CODE);
			serviceStatus.setMessage(Constant.NOT_INVOICE);
			response.setServiceStatus(serviceStatus);
			return response;
		}
		List<InvoiceDetailsModel> invodetEntityList = invoicedetailsService
				.findInvoiceDetailsById(request.getInvoiceId());
		for (InvoiceDetailsModel entity : invodetEntityList) {
			response.getInvoiceDetails().add(entity);
		}
		response.setInvoice(invoiceEntity);
		serviceStatus.setStatusCode(Constant.SUCCESS_CODE);
		response.setServiceStatus(serviceStatus);
		return response;
	}

	public boolean insertInvoice(InvoiceModel invo) throws ExceptionMessages {
		try {
			int var = 0;
			double subtotal = 0;
			if (invo.getCustomerName() == null && invo.getInvodetails().isEmpty()) {
				LOG.info("Ingrese todos los datos");
				throw new ExceptionMessages("408", "Ingrese todos los datos");
			} else {
				List<InvoiceDetailsModel> list = new ArrayList<InvoiceDetailsModel>();
				java.util.Date date = new java.util.Date();
				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy");
				var = invoiceService.getID();
				for (InvoiceDetailsModel i : invo.getInvodetails()) {
					InvoiceDetailsModel invod = new InvoiceDetailsModel();
					Product pro = Produc.findProductById(i.getProductid());
					if (pro == null) {
						LOG.info("Producto inexistente");
						throw new ExceptionMessages(Constant.ERROR_CODE, Constant.NOT_PRODUCT);
					} else if (i.getQuantity() < 1) {
						LOG.info("La cantidad tiene que ser mayor a 0");
						throw new ExceptionMessages(Constant.ERROR_CODE, Constant.PRODUCT_QUANTITY);
					} else if (i.getQuantity() > pro.getStock()) {
						LOG.info("La cantidad supera lo disponible");
						throw new ExceptionMessages(Constant.ERROR_CODE, Constant.NOT_STOCK);
					} else {
						i.setSubtotal(pro.getPrice() * i.getQuantity());
						subtotal += i.getSubtotal();
						Product prod = new Product();
						prod.setProductId(i.getProductid());
						prod.setStock(i.getQuantity());
						Produc.modify(prod);
						invod.setProductid(pro.getProductId());
						invod.setQuantity(i.getQuantity());
						invod.setSubtotal(pro.getPrice() * i.getQuantity());
						invod.setInvoiceid(var);
						list.add(invod);
					}
				}
				invo.setInvoiceId(var);
				invo.setDateInfo(sdf.format(date));
				invo.setTotal(subtotal);
				invo.setIva(Constant.IVA * subtotal);
				invo.setTotalPayment(invo.getTotal() + invo.getIva());
				invo.setStatus(true);
				invoiceService.insert(invo);
				invoicedetailsService.inserBatch(list);
			}
		} catch (ExceptionMessages e) {
			throw e;
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return true;
	}

	public DeleteInvoiceResponse delete(DeleteInvoiceRequest request) {
		DeleteInvoiceResponse response = new DeleteInvoiceResponse();
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			// 1.consultara si la factura existe
			InvoiceModel invo = invoiceService.findInvoiceById(request.getInvoiceId());
			// 2. actualizar en la BD
			invo.setStatus(false);
			boolean df = invoiceService.delete(request.getInvoiceId());
			// 3. validacion despues de la peticion
			if (df == false) {
				LOG.info("Error al  eliminar");
				serviceStatus.setStatusCode(Constant.ERROR_CODE_DELETE);
				serviceStatus.setMessage(Constant.ERROR_DELETE);
				response.setServiceStatus(serviceStatus);
				return response;
			} else {
				serviceStatus.setStatusCode(Constant.SUCCESS_CODE_DETELE);
				serviceStatus.setMessage(Constant.SUCCESS_DELETE_INVOICE);
				LOG.info("factura eliminada");
			}
		} catch (Exception e) {
			LOG.info("No se encontro la factura");
			serviceStatus.setStatusCode(Constant.ERROR_CODE);
			serviceStatus.setMessage(Constant.NOT_INVOICE);
			response.setServiceStatus(serviceStatus);
			return response;
		}
		response.setServiceStatus(serviceStatus);
		return response;
	}

	public AddInvoiceResponse addInvoice(AddInvoiceRequest request) throws ExceptionMessages {
		AddInvoiceResponse response = new AddInvoiceResponse();
		InvoiceModel invo = new InvoiceModel();
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<InvoiceDetailsModel> list = new ArrayList<InvoiceDetailsModel>();
			for (InvoiceDetailsDto i : request.getInvoiceDetailsDto()) {
				InvoiceDetailsModel invod = new InvoiceDetailsModel();
				invod.setProductid(i.getProductId());
				invod.setQuantity(i.getQuantity());
				list.add(invod);
			}
			invo.setCustomerName(request.getCustomerName());
			invo.setInvodetails(list);
			insertInvoice(invo);
			serviceStatus.setStatusCode(Constant.SUCCESS_CODE_CREATED);
			serviceStatus.setMessage(Constant.SUCCESS_INVOICE);
		} catch (ExceptionMessages e) {
			serviceStatus.setStatusCode(e.getCode());
			serviceStatus.setMessage(e.getDescription());
			response.setServiceStatus(serviceStatus);
			return response;
		}
		response.setServiceStatus(serviceStatus);
		return response;
	}
}
