package com.htc.demoinvoicing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.demoinvoicing.dao.InvoiceDao;
import com.htc.demoinvoicing.dao.InvoiceDetailsDao;
import com.htc.demoinvoicing.model.InvoiceModel;
import com.htc.demoinvoicing.service.InvoiceService;


@Service
public class InvoiceServiceImpl implements InvoiceService {
	 @Autowired 
	 InvoiceDao InvoiceDao;
	 @Autowired 
	 InvoiceDetailsDao invoicedetailsDao;
	 
	 @Override
		public boolean insert(InvoiceModel invo) {
			// TODO Auto-generated method stub
		 InvoiceDao.insert(invo);
		 return true;	
		}

	@Override
	public List<InvoiceModel>  loadAllInvoice() {
		 List<InvoiceModel> listInvo = InvoiceDao.loadAllInvoice();    
		return listInvo;
	}

	@Override
	public boolean modify(InvoiceModel invo) {
		 InvoiceDao.modify(invo.getInvoiceId(), invo);
		return true;
	}

	@Override
	public Integer getID() {
		// TODO Auto-generated method stub
		return InvoiceDao.getID();
	}

	@Override
	public InvoiceModel findInvoiceById(long invo_id) {
		// TODO Auto-generated method stub
		return InvoiceDao.findInvoiceById(invo_id);
		 
		
	}

	@Override
	public boolean delete(long invo_id) {
		// TODO Auto-generated method stub
		return InvoiceDao.delete(invo_id);
	}

}
