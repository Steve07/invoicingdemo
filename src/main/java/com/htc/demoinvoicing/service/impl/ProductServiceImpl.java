package com.htc.demoinvoicing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.htc.demoinvoicing.dao.ProductDao;
import com.htc.demoinvoicing.model.Product;
import com.htc.demoinvoicing.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao ProductDao;

	@Override
	public boolean insert(Product prod) {
		// TODO Auto-generated method stub
		return ProductDao.insert(prod);
	}

	@Override
	public boolean delete(Product prod) {
		ProductDao.delete(prod.getProductId(), prod);
		return true;
	}

	@Override
	public boolean modify(Product prod) {
		ProductDao.modify(prod.getProductId(), prod);
		return true;
	}

	@Override
	public void inserBatch(List<Product> products) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Product> loadAllProduct() {
		// TODO Auto-generated method stub
		List<Product> listProdu = ProductDao.loadAllProduct();
		return listProdu;
	}

	@Override
	public Product findProductById(long product_id) {
		// TODO Auto-generated method stub
		return ProductDao.findProductById(product_id);
	}

}
