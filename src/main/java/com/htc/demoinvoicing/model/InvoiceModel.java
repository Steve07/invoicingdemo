
package com.htc.demoinvoicing.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * 
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InvoiceDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="invoiceid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="customername" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dateinfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="total" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="iva" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="totalpayment" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoiceModel", propOrder = {
    "invoiceId",
    "customerName",
    "dateInfo",
    "total",
    "iva",
    "totalPayment"
})
public class InvoiceModel implements Serializable{

	private static final long serialVersionUID = 1L;
    protected long invoiceId;
    @XmlElement(required = true)
    protected String customerName;
    @XmlElement(required = true)
    protected String dateInfo;
    protected double total;
    protected double iva;
    @XmlTransient
    //@JsonIgnore
    protected boolean status;
    protected double totalPayment;
    @XmlTransient
    //@JsonIgnore
	private List<InvoiceDetailsModel> invoDetails;
    
    
    public InvoiceModel() {
	}

	public InvoiceModel(String customerName, String dateInfo, Double total, Double iva, Double totalPayment, List<InvoiceDetailsModel> invoDetails ) {
		this.customerName = customerName;
		this.dateInfo = dateInfo;
		this.total = total;
		this.iva = iva;
		this.totalPayment = totalPayment;
		this.invoDetails= invoDetails;
		// TODO Auto-generated constructor stub
	}

    public List<InvoiceDetailsModel> getInvodetails() {
		return invoDetails;
	}

	public void setInvodetails(List<InvoiceDetailsModel> invodetails) {
		this.invoDetails = invodetails;
	}

	/**
     * Obtiene el valor de la propiedad invoiceid.
     * 
     */
    public long getInvoiceId() {
        return invoiceId;
    }

    /**
     * Define el valor de la propiedad invoiceid.
     * 
     */
    public void setInvoiceId(long value) {
        this.invoiceId = value;
    }

    /**
     * Obtiene el valor de la propiedad customername.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Define el valor de la propiedad customername.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerName(String value) {
        this.customerName = value;
    }

    /**
     * Obtiene el valor de la propiedad dateinfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateInfo() {
        return dateInfo;
    }

    /**
     * Define el valor de la propiedad dateinfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateInfo(String value) {
        this.dateInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad total.
     * 
     */
    public double getTotal() {
        return total;
    }

    /**
     * Define el valor de la propiedad total.
     * 
     */
    public void setTotal(double value) {
        this.total = value;
    }

    /**
     * Obtiene el valor de la propiedad iva.
     * 
     */
    public double getIva() {
        return iva;
    }

    /**
     * Define el valor de la propiedad iva.
     * 
     */
    public void setIva(double value) {
        this.iva = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     */
    public void setStatus(boolean value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad totalpayment.
     * 
     */
    public double getTotalPayment() {
        return totalPayment;
    }

    /**
     * Define el valor de la propiedad totalpayment.
     * 
     */
    public void setTotalPayment(double value) {
        this.totalPayment = value;
    }
    
    @Override
	public String toString() {
		return "Numero de factura: " + invoiceId + "\nnombre Cliente: " + customerName + "\nfecha: " + dateInfo
				+ "\nTotal: "+ total+"\nIava: "+iva+"\nTotal a pagar: "+ totalPayment;
	}


}
