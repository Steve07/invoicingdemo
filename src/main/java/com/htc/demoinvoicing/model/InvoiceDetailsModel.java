package com.htc.demoinvoicing.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <p>
 * Clase Java para InvoiceDetailsLoadDto complex type.
 * 
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InvoiceDetailsLoadDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoiceDetailsModel", propOrder = { "productId", "quantity", "subtotal" })

public class InvoiceDetailsModel implements Serializable {

	private static final long serialVersionUID = 1L;
	@XmlTransient
	//@JsonIgnore
	private long invoiceDetailsId;
	@XmlTransient
	//@JsonIgnore
	private long invoiceId;
	protected long productId;
	protected int quantity;
	protected double subtotal;

	public InvoiceDetailsModel() {

	}

	public InvoiceDetailsModel(long invoiceDetailsId, long invoiceId, long productId, Integer quantity,
			Double subtotal) {
		this.invoiceDetailsId = invoiceDetailsId;
		this.invoiceId = invoiceId;
		this.productId = productId;
		this.quantity = quantity;
		this.subtotal = subtotal;

	}

	public long getInvoicedetails_id() {
		return invoiceDetailsId;
	}

	public void setInvoicedetails_id(long invoicedetailsId) {
		this.invoiceDetailsId = invoicedetailsId;
	}

	public long getInvoiceid() {
		return invoiceId;
	}

	public void setInvoiceid(long invoiceId) {
		this.invoiceId = invoiceId;
	}

	/**
	 * Obtiene el valor de la propiedad productid.
	 * 
	 */
	public long getProductid() {
		return productId;
	}

	/**
	 * Define el valor de la propiedad productid.
	 * 
	 */
	public void setProductid(long value) {
		this.productId = value;
	}

	/**
	 * Obtiene el valor de la propiedad quantity.
	 * 
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Define el valor de la propiedad quantity.
	 * 
	 */
	public void setQuantity(int value) {
		this.quantity = value;
	}

	/**
	 * Obtiene el valor de la propiedad subtotal.
	 * 
	 */
	public double getSubtotal() {
		return subtotal;
	}

	/**
	 * Define el valor de la propiedad subtotal.
	 * 
	 */
	public void setSubtotal(double value) {
		this.subtotal = value;
	}
	
	@Override
	public String toString() {
		
		return "\nProducto: " + productId +"\nCantidad: " + quantity+"\nSubtotal: "+subtotal+"\n";
	}

}
