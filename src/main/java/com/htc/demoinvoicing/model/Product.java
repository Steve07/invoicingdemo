package com.htc.demoinvoicing.model;

import java.io.Serializable;

public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	private long productId;
	private String productName;
	private double price;
	private Integer stock;
	private boolean status;
	
	
	public Product() {

	}

	public Product(long productId, String productName,Double price, Integer stock) {
		this.productId = productId;
		this.productName = productName;
		this.price = price;
		this.stock=stock;
		
	}
	
	public long getProductId() {
		return productId;
	}
	public void setProductId(long product_id) {
		this.productId = product_id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productname) {
		this.productName = productname;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	
	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Numero de producto: " + productId +"\nnombre Producto: " + productName +"\nPrecio: " + price+"\nStock: "+stock+"\n";
	}
	
}
