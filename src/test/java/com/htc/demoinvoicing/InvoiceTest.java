package com.htc.demoinvoicing;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.demoinvoicing.model.InvoiceModel;
import com.htc.demoinvoicing.service.impl.InvoiceDetailsServiceImpl;
import com.htc.demoinvoicing.service.impl.InvoiceServiceImpl;


@RunWith(SpringRunner.class)
@SpringBootTest
public class InvoiceTest extends AbstractTransactionalJUnit4SpringContextTests {


	@Autowired
	InvoiceServiceImpl invoService;
	InvoiceDetailsServiceImpl invodService;
	//InvoiceDao bilDao;

	@Test
	public void testInsertInvoice() {
		InvoiceModel bill_1 = new InvoiceModel();
		bill_1.setInvoiceId(12345);
		bill_1.setDateInfo("24/05/2019");
		bill_1.setCustomerName("rollback");
		bill_1.setTotal(00.00);
		bill_1.setIva(00.00);
		bill_1.setTotalPayment(00.00);
		
		assertEquals(true, invoService.insert(bill_1));
		
	}
	
	@Test
	public void testDelete() {
		assertEquals(true, invoService.delete(1234));
	}
	
	@Test
	public void testfindInvoiceById() {
		assertNotNull(invoService.findInvoiceById(186));
	}

}
